/**
Enumeration to symbolize the current state of a `UITableView`. Can be used to trigger actions when the table view
gets (re)loaded, shows data or is empty.
*/
enum TableViewState {
	case initial
	case empty
	case loading
	case displaying
}

