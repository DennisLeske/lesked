#!/bin/bash

#check if we're on master branch
GIT_CURRENT_BRANCH=`git rev-parse --abbrev-ref HEAD`
if [ "$GIT_CURRENT_BRANCH" != "master" ];
then
	echo "This script may only be executed on the origin/master branch. Please switch branches and try again."
	exit -1
fi

#check if the current branch is clean and has no changes
if ! git diff-index --quiet HEAD --; then
	echo "Your local branch has uncommitted changes. Please stash/commit/push/revert them and try again."
	exit -2
fi

#check if command exists
PLISTBUDDY_CMD=/usr/libexec/PlistBuddy
if [ ! -f "$PLISTBUDDY_CMD" ]; then
	echo "Could not find dependency: $PLISTBUDDY_CMD"
	exit -3
fi

#base folder where the .plist files are located
PLIST_BASEFOLDER=carsync/carsync

#list of .plist files
PLIST_FILES=(\
	"$PLIST_BASEFOLDER/Info-Production.plist" \
	"$PLIST_BASEFOLDER/Info-Development.plist" \
	"$PLIST_BASEFOLDER/Info-Porsche-Enterprise.plist" \
	"$PLIST_BASEFOLDER/Info-Porsche-TestFlight.plist" \
	"$PLIST_BASEFOLDER/Info-Porsche-Development.plist"
)

#check for plist files
for i in "${PLIST_FILES[@]}"
do
	:
	if [ ! -f "$i" ];
	then
		echo "Could not locate .plist file at $i. Aborting."
		exit -1
	fi
done

#read the current version information from the first .plist file
echo Reading current version information from ${PLIST_FILES[0]}...
CURRENT_VERSION_STRING=`$PLISTBUDDY_CMD -c "Print :CFBundleShortVersionString" ${PLIST_FILES[0]}`
CURRENT_VERSION_MAJOR=`echo $CURRENT_VERSION_STRING | cut -d'.' -f1`
CURRENT_VERSION_MINOR=`echo $CURRENT_VERSION_STRING | cut -d'.' -f2`
CURRENT_VERSION_REVISION=`echo $CURRENT_VERSION_STRING | cut -d'.' -f3`
CURRENT_BUNDLE_VERSION=`$PLISTBUDDY_CMD -c "Print :CFBundleVersion" ${PLIST_FILES[0]}`
echo Version string: $CURRENT_VERSION_STRING, bundle version: $CURRENT_BUNDLE_VERSION

#build the next version numbers
NEXT_VERSION_MAJOR=$(($CURRENT_VERSION_MAJOR+1))
NEXT_VERSION_MINOR=$(($CURRENT_VERSION_MINOR+1))
NEXT_VERSION_REVISION=$(($CURRENT_VERSION_REVISION+1))
NEXT_VERSION_1="$NEXT_VERSION_MAJOR.0.0"
NEXT_VERSION_2="$CURRENT_VERSION_MAJOR.$NEXT_VERSION_MINOR.0"
NEXT_VERSION_3="$CURRENT_VERSION_MAJOR.$CURRENT_VERSION_MINOR.$NEXT_VERSION_REVISION"
NEXT_BUNDLE_VERSION=$((CURRENT_BUNDLE_VERSION+1))

#show menu for the version string
echo
echo "Choose next version string:"
echo "	1) $NEXT_VERSION_1 (increase major, reset others)"
echo " 	2) $NEXT_VERSION_2 (increase minor, reset revision)"
echo " 	3) $NEXT_VERSION_3 (increase revision)"
echo "	4) Enter custom version string"
echo "	5) Cancel"

options=("1" "2" "3" "4" "5")
read -p "Your selection: " option
case $option in
	"1")
		NEXT_VERSION_STRING=$NEXT_VERSION_1
		;;
	"2")
		NEXT_VERSION_STRING=$NEXT_VERSION_2
		;;
	"3")
		NEXT_VERSION_STRING=$NEXT_VERSION_3
		;;
	"4")
		read -p "Version string: " version_string
		NEXT_VERSION_STRING=$version_string
		;;
	"5")
		echo "Aborted by user"
		exit 0
		;;
	*)
		echo "Input error: invalid option. Aborting."
		exit -1
		;;
esac

#show menu for bundle version
echo
echo "Choose next bundle version:"
echo "	1) $NEXT_BUNDLE_VERSION"
echo "	2) Enter custom bundle version"
echo "	3) Cancel"

options=("1" "2" "3")
read -p "Your selection: " option
case $option in
	"1")
		#no changes to NEXT_BUNDLE_VERSION
		;;
	"2")
		read -p "Bundle version: " bundle_version
		NEXT_BUNDLE_VERSION=$bundle_version
		;;
	"3")
		echo "Aborted by user"
		exit 0
		;;
	*)
		echo "Input error: invalid option. Aborting."
		exit -1
		;;
esac

#start plist operations
echo
for i in "${PLIST_FILES[@]}"
do
	:
	echo "Changing file $i..."
	$PLISTBUDDY_CMD -c "Set :CFBundleVersion $NEXT_BUNDLE_VERSION" $i
	$PLISTBUDDY_CMD -c "Set :CFBundleShortVersionString $NEXT_VERSION_STRING" $i
done

echo
echo "Changes have been written. Do you want to commit, push and tag?"

read -p "(y|n) " confirm
case $confirm in
	"n")
		exit 0
		;;
	"y")
		##continue below
		;;
	*)
		echo "Input error: invalid option. Aborting."
		exit -1
		;;
esac

echo
echo "Committing, pushing and tagging..."

GIT_COMMIT_MESSAGE="setting version information for $NEXT_VERSION_STRING ($NEXT_BUNDLE_VERSION)"
GIT_TAG="${NEXT_BUNDLE_VERSION}_${NEXT_VERSION_STRING}"
for i in "${PLIST_FILES[@]}"
do
	:
	git add $i
done
git commit -m "$GIT_COMMIT_MESSAGE"
git push
git tag -a "$GIT_TAG" -m "$GIT_COMMIT_MESSAGE"
git push origin "$GIT_TAG"

echo "All done!"
