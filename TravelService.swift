//
//  TravelService.swift
//  carsync
//
//  Created by Dennis Leske on 13.04.17.
//  Copyright © 2017 Vispiron ENGINEERING GmbH. All rights reserved.
//

import Foundation
import Alamofire

/// Filter key that can be used when filtering travels
enum TravelFilterKey {
	case workflowState
	case useType
	case order
	case startTimeMin
	case startTimeMax
	
	var stringValue: String {
		switch self {
		case .workflowState:
			return "workflowState"
		case .useType:
			return "useType"
		case .order:
			return "order"
		case .startTimeMin:
			return "startTimeMin"
		case .startTimeMax:
			return "startTimeMax"
		}
	}
}

struct TravelOrder {
	
	enum Key {
		case startTime
		case endTime
		var stringValue: String {
			switch self {
			case .startTime: return "startTime"
			case .endTime: return "endTime"
			}
		}
	}
	
	enum Direction {
		case ascending
		case descending
		var stringValue: String {
			switch self {
			case .ascending: return "ASC"
			case .descending: return "DESC"
			}
		}
	}
	
	var key: Key
	var direction: Direction
	
	var stringValue: String {
		return "\(key.stringValue) \(direction.stringValue)"
	}
	
}

/**
Filter object.

Usage example:

	let filter = TravelFilter(workflowStates: [.accepted], useTypes: [.business, .privat])
	TravelService.sharedInstance.getTravels(clear: true, filter: filter) { (travels, error) in ... }

*/
struct TravelFilter {
	var workflowStates: [WorkflowState]?
	var useTypes: [Travel.UseType]?
	var order: TravelOrder?
	var startTimeMin: Date?
	var startTimeMax: Date?
	
	func buildRESTFilter() -> [String:Any] {
		var result: [String: Any] = [:]
		if let workflowStates = workflowStates {
			result[TravelFilterKey.workflowState.stringValue] = workflowStates
		}
		if let useTypes = useTypes {
			result[TravelFilterKey.useType.stringValue] = useTypes
		}
		if let startTimeMin = startTimeMin {
			result[TravelFilterKey.startTimeMin.stringValue] = startTimeMin
		}
		if let startTimeMax = startTimeMax {
			result[TravelFilterKey.startTimeMax.stringValue] = startTimeMax
		}
		if let order = order {
			result[TravelFilterKey.order.stringValue] = order.stringValue
		}
		return result
	}
}

class TravelService {
	
	static let sharedInstance = TravelService()
	
	/**
	Read travels from the backend and calls the completion block with the result.
	- Parameter onComplete: Completion block to call
	*/
	func getTravels(clear: Bool, _ onComplete: @escaping (_ travels: [Travel]?, _ error: NSError?) -> Void) {
		RESTService.shared.getAll(clear: clear) { (travels, error) in
			onComplete(travels, error)
		}
	}
	
	/**
	Read travels from the backend, using the given filter.
	- Parameter clear: `true` if the request should reset the content range and get items from the beginning
	- Parameter filter: the filter to use when sending the request
	- Parameter onComplete: completion block to call
	*/
	func getTravels(clear: Bool, filter: TravelFilter, _ onComplete: @escaping (_ travels: [Travel]?, _ error: NSError?) -> Void) {
		RESTService.shared.getFiltered(clear: clear, filter.buildRESTFilter()) { (travels, error) in
			onComplete(travels, error)
		}
	}

	/**
	Do a POST request to the travels API to create a new travel object.
	- Parameter travel: `Travel` object
	- Parameter onComplete: Completion block
	*/
	func createTravel(travel: Travel, _ onComplete: @escaping (_ travel: Travel?, _ error: NSError?) -> Void) {
		RESTService.shared.post(object: travel, context: .dontCare) { (travel, error) in
			onComplete(travel, error)
		}
	}
	
	/**
	PUT the changes of a travel. Can be used for confirmation or editing.
	- Parameter travel: `Travel` to update
	- Parameter onComplete: Completion block
	*/
	func updateTravel(travel: Travel, _ onComplete: @escaping (_ travel: Travel?, _ error: NSError?) -> Void) {
		RESTService.shared.put(travel, useObservedProperties: false) { (travel, error) in
			onComplete(travel, error)
		}
	}

	/**
	Get a list of travel possibilities for the given travel id.
	- Parameter travelId: ID of the `Travel` to get the possibilities for (should be the value of a travels' idTempTravel when editing)
	- Parameter onComplete: Completion block
	*/
	func getPossibilities(forTravelId travelId: Int, _ onComplete: @escaping (_ possibilites: [TravelPossibility]?, _ error: NSError?) -> Void) {
		let filter: RESTService.RESTFilter = [ "idTravel": travelId ]
		RESTService.shared.getFiltered(clear: true, filter) { (possibilities: [TravelPossibility]?, error: NSError?) in
			onComplete(possibilities, error)
		}
	}
	
	func delete(travel: Travel, _ onComplete: @escaping(_ error: NSError?) -> Void) {
		// sending a DELETE request doesn't really delete a travel, it just changes its workflow state
		RESTService.shared.delete(travel, context: .dontCare) { (error) in
			onComplete(error)
		}
	}

}
