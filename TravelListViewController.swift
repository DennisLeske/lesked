//
//  TravelListViewController
//  carsync
//
//  Created by Dennis Leske on 27.03.17.
//  Copyright © 2017 Vispiron ENGINEERING GmbH. All rights reserved.
//

import UIKit
import MGSwipeTableCell

private let cellIdentifier = "TravelListCell"

private enum DisplayMode {
	case current
	case history
}

class TravelListViewController: AbstractComponentViewController {
	
	// MARK: - Properties
	
	/// the current state (loading, displaying, ...) of the table view
	private var tableviewState: TableViewState? {
		didSet {
			setComponentVisibilityState()
		}
	}
	
	/// the current display mode (history or current)
	fileprivate var displayMode: DisplayMode = .current
	
	/// flag to check if the current user is a field manager (resp. is NOT)
	fileprivate var currentUserIsFm: Bool {
		if let isFm = RootManager.sharedInstance.userManager.bookingConfig?.isFm {
			return isFm
		}
		return false
	}
	
	/// the current filter setting
	private var travelFilter: TravelFilter!
	
	/// string for the loading indicator
	fileprivate var loadingHandler: String?
	
	/// can be set by child view controllers, indicates if the travel list should reload when it appears
	public var shouldReloadOnAppear: Bool = true
	
	// MARK: - IBOutlets
	
	@IBOutlet weak var lbl_header: UILabel!
	@IBOutlet weak var btn_history: CarsyncImageButton!
	@IBOutlet weak var btn_addTravel: CarsyncImageButton!
	@IBOutlet weak var lbl_noTravelsInfo: UILabel!
	@IBOutlet weak var tv_travels: UITableView!
	
	// MARK: - UIViewController overrides

    override func viewDidLoad() {
        super.viewDidLoad()

		if isIpad {
			MainNavigationManager.sharedInstance.mainIpadViewController = self
		}
		
		// override for AbstractComponentViewController
		isNavigationRoot = displayMode == .current
		
		// set the current filter
		// only load travels that have been accepted; other travels may not have the necessary data to display (e.g. booking, vehicle)
		travelFilter = TravelFilter()
		travelFilter.workflowStates = [ .accepted ]
		travelFilter.useTypes = [.business, .private]
		if displayMode == .current {
			travelFilter.order = TravelOrder(key: .startTime, direction: .ascending)
			travelFilter.startTimeMin = Date()
		} else {
			travelFilter.order = TravelOrder(key: .startTime, direction: .descending)
			travelFilter.startTimeMax = Date()
		}
		
		// Add infinite scroll handler
		self.tv_travels.addInfiniteScroll { (scrollView) -> Void in
			let tableView = scrollView
			TravelService.sharedInstance.getTravels(clear: false, filter: self.travelFilter) { (travels, error) in
				tableView.finishInfiniteScroll()
				self.onTravelsLoaded(travels: travels, error: error)
			}
		}
		
        localizeUI()
		applyAppearance()

		insertPullRefresh()
		
		tableviewState = .initial
		
		if displayMode == .history {
			tableviewState = .loading
			loadingHandler = LoadingViewController.pushLoadingViewControllerWithLoadingText("")
			loadTravels()
		}
    }
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		if displayMode == .current && shouldReloadOnAppear {
			tableviewState = .loading
			loadingHandler = LoadingViewController.pushLoadingViewControllerWithLoadingText("")
			loadTravels()
		}
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
	
	// MARK: - AbstractComponentViewController overrides
	
	override func defineHeaderType() -> HeaderType {
		return HeaderType.defaultNoSearch
	}
	
	override func handlePop(_ navigtionViewController: UINavigationController?) {
		// only allow popping in history mode
		if displayMode == .history {
			let _ = navigtionViewController?.popViewController(animated: true)
		}
	}
	
	// MARK: - UI and appearance
	
	func localizeUI() {
		headerName = NSLocalizedString("bookings", comment: "header for bookings feature")
		if displayMode == .current {
			lbl_header.text = NSLocalizedString("bookings overview", comment: "header for list of all bookings")
			btn_history.setTitle(NSLocalizedString("history", comment: ""), for: .normal)
			btn_addTravel.setTitle(NSLocalizedString("new", comment: ""), for: .normal)
		} else {
			lbl_header.text = NSLocalizedString("bookings history", comment: "header for list of historic bookings")
		}
		lbl_noTravelsInfo.text = NSLocalizedString("No bookings found.", comment: "Text indicating that no travels could be found")
	}
	
	func applyAppearance() {
		btn_addTravel.isHidden = displayMode == .history
		btn_history.isHidden = displayMode == .history
	}
	
	func setComponentVisibilityState() {
		guard let state = tableviewState else {
			return
		}
		switch state {
			
		case .initial:
			lbl_noTravelsInfo.isHidden = true
			tv_travels.isHidden = true
			
		case .empty:
			lbl_noTravelsInfo.isHidden = false
			tv_travels.isHidden = false
			
		case .displaying:
			lbl_noTravelsInfo.isHidden = true
			tv_travels.isHidden = false
			
		case .loading:
			lbl_noTravelsInfo.isHidden = true
			tv_travels.isHidden = false
			
		}
	}
	
	func insertPullRefresh() {
		let pullToRefresh = CarsyncPullToRefresh(frame: CGRect(x: 0.0, y: 10.0, width: self.view.frame.size.width, height: 100.0))
		tv_travels.addPullToRefresh(pullToRefresh) { [weak self] in
			let delayTime = DispatchTime.now() + Double(Int64(2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
			DispatchQueue.main.asyncAfter(deadline: delayTime) {
				self?.tv_travels.endRefreshing(at: .top)
				
				// force reload from backend explicitly
				self?.tableviewState = .loading
				self?.loadingHandler = LoadingViewController.pushLoadingViewControllerWithLoadingText("")
				self?.loadTravels()
			}
		}
	}
	
	// MARK: - IBActions

	@IBAction func buttonHistoryTap(_ sender: UIButton) {
		// push this view controller with a different display mode
		let vc = (StoryboardHelper.travelStoryboard().instantiateViewController(withIdentifier: "TravelListViewController") as! TravelListViewController)
		vc.displayMode = .history
		self.navigationDelegate.changedInsightMainContent(vc)
		self.navigationController?.pushViewController(vc, animated: true)
		
		// disable reloading when popping to the list again
		shouldReloadOnAppear = false
	}
	
	/**
	Opens the `TravelFormViewController`s view.
	*/
	@IBAction func btnNewTravelTap(_ sender: UIButton) {
		// check the bookingconfigsettings for allowance of the workflow state
		if let travelLogic = RootManager.sharedInstance.userManager.bookingConfig?.settings?.travelLogic,
			travelLogic.isAllowedTransitionForTraveler(from: .new, to: .draft) || travelLogic.isAllowedTransitionForFieldManager(from: .new, to: .draft) {
			openNewTravelForm()
		} else {
			logger.debug("error: changing state to draft is not allowed")
			let message = NSLocalizedString("Creating a new booking is not allowed.", comment: "Error message indicating insufficient rights for the user to create a new booking.")
			PopupHelper.showErrorOverlayer(NSLocalizedString("error", comment: ""), message: message, completionBlock: nil)
		}
	}
	
    // MARK: - Navigation
	
	/**
	Open the form to request new travels. Uses the `TravelFormViewController` on iPhones and
	the `TravelRequestViewController` (see IpadAdaptions.storyboard) for iPads.
	*/
	func openNewTravelForm() {
		// refresh the list of options we might need for the booking form
		loadingHandler = LoadingViewController.pushLoadingViewControllerWithLoadingText("")
		LoadingViewController.popLoadingViewController(self.loadingHandler!)
		
		// push TravelFormViewController
		let formVC = (StoryboardHelper.travelStoryboard().instantiateViewController(withIdentifier: "TravelFormViewController") as! TravelFormViewController)
		self.navigationDelegate.changedInsightMainContent(formVC)
		self.navigationController?.pushViewController(formVC, animated: true)
		// necessary?
		// self.navigationController?.addChildViewController(formVC)
		// formVC.didMove(toParentViewController: navigationController)
		
		// disable reloading when popping to the list again; TravelPossibilityViewController sets this to true to make
		// the list reload with the new travel data
		shouldReloadOnAppear = false
	}
	
	// MARK: - Data
	
	func loadTravels() {
		if displayMode == .current {
			RootManager.sharedInstance.travelManager.travels = []
		} else {
			RootManager.sharedInstance.travelManager.travelsHistory = []
		}
		TravelService.sharedInstance.getTravels(clear: true, filter: travelFilter) { (travels, error) in
			self.onTravelsLoaded(travels: travels, error: error)
		}
	}
	
	func onTravelsLoaded(travels: [Travel]?, error: NSError?) {
		LoadingViewController.popLoadingViewController(self.loadingHandler!)
		
		if let error = error {
			logger.debug("error: \(error)")
			// NSLocalizedString("unknown server error", comment: "")
		} else if let travels = travels {
			logger.debug("got \(travels.count) travels")
			// combine new travels with existing ones
			if displayMode == .current {
				RootManager.sharedInstance.travelManager.travels += travels
			} else {
				RootManager.sharedInstance.travelManager.travelsHistory += travels
			}
			
			let travelCountTotal = displayMode == .current ? RootManager.sharedInstance.travelManager.travels.count : RootManager.sharedInstance.travelManager.travelsHistory.count
			
			self.tableviewState = travelCountTotal == 0 ? .empty : .displaying
			self.tv_travels.reloadData()
		}
	}
	
}

// MARK: - UITableViewDataSource implementation

extension TravelListViewController: UITableViewDataSource {

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return displayMode == .current ? RootManager.sharedInstance.travelManager.travels.count : RootManager.sharedInstance.travelManager.travelsHistory.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tv_travels.dequeueReusableCell(withIdentifier: cellIdentifier) as! TravelListTableViewCell
		
		if displayMode == .current {
			let travel = RootManager.sharedInstance.travelManager.travels[indexPath.row]
			cell.travel = travel
			
			// allow swipe to delete if applicable
			if let _ = travel.targetStates?.index(of: .deleted) {
				cell.delegate = self
				cell.rightButtons = [
					MGSwipeButton(title: NSLocalizedString("Delete", comment: ""), backgroundColor: .red)
				]
			}
		} else {
			let travel = RootManager.sharedInstance.travelManager.travelsHistory[indexPath.row]
			cell.travel = travel
		}
		
		// if the current user is NOT a fleet manager, remove the driver label and resize the content
		// and the disclosure
		if !currentUserIsFm {
			cell.c_contentHeight.constant -= cell.c_lblTravelerHeight.constant
			cell.c_lblTravelerHeight.constant = 0.0
			cell.lblTraveler.isHidden = true
		}

		return cell
	}
	
}

// MARK: - UITableViewDelegate implementation

extension TravelListViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		// fleet managers can see the travelers name, the row needs to have another height in that case
		return 195.0 + ( currentUserIsFm ? 25.0 : 0.0 )
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		// push TravelDetailViewController
		let vc = (StoryboardHelper.travelStoryboard().instantiateViewController(withIdentifier: "TravelDetailViewController") as! TravelDetailViewController)
		vc.isEditable = displayMode == .current
		navigationDelegate.changedInsightMainContent(vc)
		navigationController?.pushViewController(vc, animated: true)
		shouldReloadOnAppear = false
		
		if displayMode == .current {
			vc.travel = RootManager.sharedInstance.travelManager.travels[indexPath.row]
		} else {
			vc.travel = RootManager.sharedInstance.travelManager.travelsHistory[indexPath.row]
		}
		
	}
	
}

// MARK: - MGSwipeTableCellDelegate implementation

extension TravelListViewController: MGSwipeTableCellDelegate {
	
	func swipeTableCell(_ cell: MGSwipeTableCell, tappedButtonAt index: Int, direction: MGSwipeDirection, fromExpansion: Bool) -> Bool {
		if index == 0 {
			// delete
			let confirm = UIAlertController(
				title: NSLocalizedString("Confirmation", comment: ""),
				message: NSLocalizedString("Do you really want to delete the booking?", comment: ""),
				preferredStyle: .alert)
			confirm.addAction(UIAlertAction.init(title: NSLocalizedString("yes", comment: ""), style: .destructive, handler: { (action) in
				self.loadingHandler = LoadingViewController.pushLoadingViewControllerWithLoadingText("")
				let travel = (cell as! TravelListTableViewCell).travel!
				RootManager.sharedInstance.travelManager.delete(travel: travel, { (error) in
					PopupHelper.showSaveHintOverlayer(NSLocalizedString("Booking has been deleted", comment: ""), completionBlock: nil, duration: 1.0)
					self.loadTravels()
				})
			}))
			confirm.addAction(UIAlertAction.init(title: NSLocalizedString("no", comment: ""), style: .cancel, handler: { (action) in
			}))
			self.present(confirm, animated: true, completion: nil)
		}
		return true
	}
	
}
